import argparse

argparser = argparse.ArgumentParser(description='Arguments that may be parsed.',epilog="The Epilog")
argparser.add_argument('--test',action='store_true',help='Simulate the Run')
argparser.add_argument('--dir','-d',type=str,required=True,help='Required variable "dir" with the following text as its value. i.e. --dir /test/123/ ')
argparser.add_argument('--optionaldefault','-o',default="the default value",type=str,help='Creates a variable "optionaldefault"  ')
argparser.add_argument('--integer','-i',default=0,type=int,help='Creates a variable "integer" with the value 0 if nothing is specified.')
args = argparser.parse_args()


def main(object args):
  pass


if __name__ == "__main__":
    try:
main(args)
  except(Exception):
    raise
    else:
    pass
    finally:
    pass
