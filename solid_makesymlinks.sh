#!/bin/bash
############################
# .make.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
############################

########## Variables

dir=~/dotfiles                    # dotfiles directory
olddir=~/dotfiles_old             # old dotfiles backup directory
#files="bashrc vimrc vim zshrc oh-my-zsh private scrotwm.conf Xresources"    # list of files/folders to symlink in homedir
files="bashrc tmux.conf init.vim vimrc vim"    # list of files/folders to symlink in homedir

##########

# create dotfiles_old in homedir
if [[ ! -d $olddir ]]; then
    echo -n "Creating $olddir for backup of any existing dotfiles in ~ ..."
    mkdir -p $olddir
    echo "done"
fi

# change to the dotfiles directory
echo -n "Changing to the $dir directory ..."
cd $dir
echo "done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks from the homedir to any files in the ~/dotfiles directory specified in $files
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    if [[ $file == 'init.vim' ]]; then
        mkdir -p ~/.config/nvim/
        mv ~/.config/nvim/$file ~/dotfiles_old/
	ln -s $dir/$file ~/.config/nvim/$file
	if [ -f /bin/nvim -o -f /usr/bin/nvim ]; then
	    echo "Neovim may not be installed..."
	fi
    else
        mv ~/.$file ~/dotfiles_old/
        echo "Creating symlink to $file in home directory."
        ln -s $dir/$file ~/.$file
    fi
done

if [[ ! -f  ~/.config/nvim/autoload/plug.vim ]]; then
    curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi


